// select a database
use <database name>

// when creating a new database via the command line, the use command can be entered with a name of a database that does not yet exist. 
// Once a new record is inserted into that database, the database will be created.

// database = filing cabinet
// collection = drawer
// document/record = folder inside a drawer
// sub-documents (optional) = other files
// fields = file/folder content

/* e.g. document with no sub-documents:
	{name: "Jino Yumul",
	age: 33,
	occupation: "Instructor"
	}

	e.g. document with sub-documents:
	{name: "Jino Yumul",
	age: 33,
	occupation: "Instructor",
	address: {
		street: "123 Street",
		city:"Makati",
		country: "Philippines"
	}
*/

/*
Embedded vs. Referenced data:

Referenced Data: 
 - preferred 
 - more compact

Users:
{
	id: 298,
	name: "Jino Yumul",
	age: 33,
	occupation: "Instructor",
}

Orders:
{
	products: [{
		name: "New Pillow",
		price: 300}],
	userID: 298
}

Embedded data:
 - included in the object
 - bloated

Users: {
	id: 298,
	name: "Jino Yumul",
	age: 33,
	occupation: "Instructor",
	orders: [{
		products: [{
			name: "New Pillow",
			price: 300}],
	}]
}
*/

// [CRUD]

// Insert One Document (Create)
/*
database -> user collection -> document
*/

db.users.insert({
	firstName:"Jane",
	lastName: "Doe",
	age: 21,
	contact:{
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})


// if inserting/creating a new document within a collection that does not yet exist, MongoDB will automatically create that collection

// insert many 

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact:{
			phone: "123456789",
			email: "stephenhawking@mail.com",
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "123456789",
			email: "neilarmstrong@mail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
])

// Finding documents (Read)

// Retrieve a list of all users
db.users.find()

// finds any and all matches
db.users.find({firstName: "Stephen"})

// find only the first match 

db.users.findOne({firstName: "Stephen"})

// finds documents with multiple parameters/conditions
db.users.find({lastName: "Armstrong", age: 82})


// Edit Documents (Update)

// create a new document to update
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact:{
		phone: "123456789",
		email: "test@mail.com"
	},
	courses: [],
	department: "none"
})

// updateOne Always needs 2 objects.
// first object: specifies which document/record will be updated
// second object: contains the $set operator and inside the $set operator are the specific fields to be updated
db.users.updateOne(
	{_id: ObjectId("62876c3f3941cfe50844df15")},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "123456789",
				email: "billgates@mail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}
	}
)

// update multiple documents

db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"}
		}
)

// deleting a single document

db.users.deleteOne({
	firstName: "test"
})

// deleting many records
// Be careful when using deleteMany. If no search criteria are provided, it will delete ALL documents within the given collection

db.users.deleteMany({
	firstName: "Bill"
})





